# README #

## ECB Currency Exchange Rates Sample App ##

### Repository structure ###

* [master](https://bitbucket.org/vk_eurodk/currencies/branch/master) branch contains only this README.md
* [symfony-install](https://bitbucket.org/vk_eurodk/currencies/branch/symfony-install#diff) contains Symfony, its components and Webpack Encore with React.js
* [ui](https://bitbucket.org/vk_eurodk/currencies/branch/ui#diff) contains working application based on __symfony-install__
* [Pull request](https://bitbucket.org/vk_eurodk/currencies/pull-requests/1) contains diff between __ui__ and __symfony-install__ - my and open-source code added
* [docker](https://bitbucket.org/vk_eurodk/currencies/branch/docker#diff) branch contains project structure required to launch it with docker

__You can clone or download any of these branches at [the repository's main page](https://bitbucket.org/vk_eurodk/currencies/src/master/)__

Just select proper branch in branches drop-down and click __Clone__ or __...__ > __Download repository__ to get the files

### Docker setup ###

* Download __docker__ branch to any folder
* It consists of __currencies__ folder that contains docker images in __currencies/docker__ folder and empty __currencies/src__ folder
* Clone or download __ui__ branch in __currencies/src__ folder. It will be used by docker php-fpm image launching composer to install dependencies in __currencies/src/vendor__ folder
* Install docker and docker-compose if not yet
* Open command prompt, go to __currencies/docker__ folder and enter __docker-compose__ __up__
* Wait for images to build and __php-fpm__ image to output __NOTICE: ready to handle connections__

### Setting up database ###

* Open a new command prompt, go to __currencies/docker__ folder and enter __docker-compose__ __exec__ __php-fpm__ __bash__
* In the image's shell enter __php__ __bin/console__ __app:init-storage__
* Make sure no warnings arise, the command just finishes silently

### Using the sample app ###

* Open __http://localhost/__ in some browser. You should get all-currencies list's 1st page. Test, if its functionality meets requirements
* At __15:00 pm__ every __working__ day new currency rates should be downloaded. You will get one more page at all-currencies view. It will be the brand-new first page.

### Who do I talk to? ###

Valerijs Krizevics
__[+371 29548048](+371 29548048)__
__[vkrizevics@gmail.com](vkrizevics@gmail.com)__